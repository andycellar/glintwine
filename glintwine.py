#!/usr/bin/python3

# Written in Python 3.6.4 and Tkinter 8.6
# This tool is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY.

from tkinter import *
from tkinter import messagebox
import os
import shutil

version = "07062021"

tbl = Tk()

homedir = os.path.expanduser("~")
winepath = "/usr/local/bin/wine"
winepath64 = "/usr/local/bin/wine64"
prefixdir = "/.wine"
steampath = "/drive_c/Program Files/Steam"
steampath64 = "/drive_c/Program Files (x86)/Steam"

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    if "#arch=win32" in open(homedir + prefixdir + "/user.reg").read():
        tbl.title("Glintwine (32-bit prefix)")
    else:
        tbl.title("Glintwine (64-bit prefix)")
else:
    tbl.title("Glintwine")

def refresh_gui():
    tbl.after(4580, tbl.destroy()) # Should be enough
    os.system("python3 " + homedir + "/glintwine/glintwine.py") # Default path

def about():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")
    lines = ["Glintwine is an open source GUI tool that provides managing of registry keys for Wine.\n", "Version:  %s\n" % str(version), "Copyright \N{COPYRIGHT SIGN} 2018-2021 Andrey Gusev"]
    messagebox.showinfo("About", "\n" . join(lines))

menubar = Menu(tbl)
menubar = Menu(menubar, tearoff = 0)
menubar.add_command(label = "Exit", command = tbl.destroy)
menubar.add_command(label = "About", command = about)
tbl.config(menu = menubar)

lf1 = LabelFrame(tbl, text = "General", font = ("Carlito", 12, "bold"))
lf1.grid(row = 0, column = 1, columnspan = 3, padx = 5, pady = 5, sticky = N+S+E+W)

lf2 = LabelFrame(tbl, text = "CSMT", font = ("Carlito", 12, "bold"))
lf2.grid(row = 1, column = 1, padx = 5, pady = 5, sticky = N+S+E+W)

lf3 = LabelFrame(tbl, text = "MultisampleTextures", font = ("Carlito", 12, "bold"))
lf3.grid(row = 1, column = 2, padx = 5, pady = 5, sticky = N+S+E+W)

lf4 = LabelFrame(tbl, text = "StrictShaderMath", font = ("Carlito", 12, "bold"))
lf4.grid(row = 1, column = 3, padx = 5, pady = 5, sticky = N+S+E+W)

lf5 = LabelFrame(tbl, text = "gameoverlayrenderer", font = ("Carlito", 12, "bold"))
lf5.grid(row = 2, column = 1, padx = 5, pady = 5, sticky = N+S+E+W)

lf6 = LabelFrame(tbl, text = "gameoverlayrenderer64", font = ("Carlito", 12, "bold"))
lf6.grid(row = 2, column = 2, padx = 5, pady = 5, sticky = N+S+E+W)

lf7 = LabelFrame(tbl, text = "GrabFullscreen", font = ("Carlito", 12, "bold"))
lf7.grid(row = 2, column = 3, padx = 5, pady = 5, sticky = N+S+E+W)

lf8 = LabelFrame(tbl, text = "d3dcompiler_**", font = ("Carlito", 12, "bold"))
lf8.grid(row = 3, column = 1, padx = 5, pady = 5, sticky = N+S+E+W)

lf9 = LabelFrame(tbl, text = "d3dx9_**", font = ("Carlito", 12, "bold"))
lf9.grid(row = 3, column = 2, padx = 5, pady = 5, sticky = N+S+E+W)

lf10 = LabelFrame(tbl, text = "d3dx10_**", font = ("Carlito", 12, "bold"))
lf10.grid(row = 3, column = 3, padx = 5, pady = 5, sticky = N+S+E+W)

lf11 = LabelFrame(tbl, text = "x3daudio*_*", font = ("Carlito", 12, "bold"))
lf11.grid(row = 4, column = 1, padx = 5, pady = 5, sticky = N+S+E+W)

lf12 = LabelFrame(tbl, text = "xapofx*_*", font = ("Carlito", 12, "bold"))
lf12.grid(row = 4, column = 2, padx = 5, pady = 5, sticky = N+S+E+W)

lf13 = LabelFrame(tbl, text = "xaudio*_*", font = ("Carlito", 12, "bold"))
lf13.grid(row = 4, column = 3, padx = 5, pady = 5, sticky = N+S+E+W)

lf14 = LabelFrame(tbl, text = "xactengine*_*", font = ("Carlito", 12, "bold"))
lf14.grid(row = 5, column = 1, padx = 5, pady = 5, sticky = N+S+E+W)

lf15 = LabelFrame(tbl, text = "mf*", font = ("Carlito", 12, "bold"))
lf15.grid(row = 5, column = 2, padx = 5, pady = 5, sticky = N+S+E+W)

lf16 = LabelFrame(tbl, text = "vkd3d", font = ("Carlito", 12, "bold"))
lf16.grid(row = 5, column = 3, padx = 5, pady = 5, sticky = N+S+E+W)

lf17 = LabelFrame(tbl, text = "VideoMemorySize (MB)", font = ("Carlito", 12, "bold"))
lf17.grid(row = 6, column = 1, columnspan = 3, padx = 5, pady = 5, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def create_prefix():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if os.path.exists(winepath64) == True:
       answer = messagebox.askyesnocancel("Creating prefix", "Do you want to create 64-bit prefix?")
       if answer == True:
          print("---\nGlintwine message: Creating 64-bit prefix.\n---")
          os.system("WINEPREFIX=" + homedir + prefixdir + " winecfg")
          refresh_gui()
       if answer == False:
          print("---\nGlintwine message: Creating 32-bit prefix in WoW64.\n---")
          os.system("WINEARCH=win32 WINEPREFIX=" + homedir + prefixdir + " winecfg")
          refresh_gui()
       else:
           return

    if os.path.exists(winepath64) == False:
       if messagebox.askyesno("Creating prefix", "Do you want to create 32-bit prefix?") == True:
          print("---\nGlintwine message: Creating 32-bit prefix.\n---")
          os.system("WINEPREFIX=" + homedir + prefixdir + " winecfg")
          refresh_gui()

if os.path.exists(winepath) == True:
    btn = Button(lf1, text = "Create prefix", width = 14, borderwidth = 2, relief = "groove", command = create_prefix)
    btn.config(font = ("Carlito", 11, "bold"))
    btn.config(fg = "#FFFFFF", bg = "#397C24", activeforeground = "#FFFFFF", activebackground = "#428E2A")
    btn.grid(row = 1, column = 1, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf1, text = "Create prefix", width = 14, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 11, "bold"))
    btn.grid(row = 1, column = 1, padx = 3, pady = 3, sticky = N+S+E+W)
if os.path.exists(homedir + prefixdir) == True:
    btn = Button(lf1, text = "Create prefix", width = 14, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 11, "bold"))
    btn.grid(row = 1, column = 1, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def winecfg():
    os.system("wine winecfg")

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    btn = Button(lf1, text = "Configuration", borderwidth = 2, relief = "groove", command = winecfg)
    btn.grid(row = 2, column = 1, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf1, text = "Configuration", borderwidth = 2, relief = "groove", state = "disabled")
    btn.grid(row = 2, column = 1, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def regedit():
    os.system("wine regedit")

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    btn = Button(lf1, text = "Registry editor", width = 14, borderwidth = 2, relief = "groove", command = regedit)
    btn.grid(row = 1, column = 2, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf1, text = "Registry editor", width = 14, borderwidth = 2, relief = "groove", state = "disabled")
    btn.grid(row = 1, column = 2, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def taskmgr():
    os.system("wine taskmgr")

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    btn = Button(lf1, text = "Task manager", borderwidth = 2, relief = "groove", command = taskmgr)
    btn.grid(row = 2, column = 2, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf1, text = "Task manager", borderwidth = 2, relief = "groove", state = "disabled")
    btn.grid(row = 2, column = 2, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def uninstaller():
    os.system("wine uninstaller")

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    btn = Button(lf1, text = "Program uninstaller", width = 15, borderwidth = 2, relief = "groove", command = uninstaller)
    btn.grid(row = 1, column = 3, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf1, text = "Program uninstaller", width = 15, borderwidth = 2, relief = "groove", state = "disabled")
    btn.grid(row = 1, column = 3, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def killserver():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Stopping wineserver", "Do you want to stop all processes?") == True:
       os.system("wineserver -k")
       tbl.destroy()

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    btn = Button(lf1, text = "Stop wineserver", borderwidth = 2, relief = "groove", command = killserver)
    btn.config(font = ("Carlito", 11, "bold"))
    btn.config(fg = "#FFFFFF", bg = "#D66119", activeforeground = "#FFFFFF", activebackground = "#E56414")
    btn.grid(row = 2, column = 3, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf1, text = "Stop wineserver", borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 11, "bold"))
    btn.grid(row = 2, column = 3, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def delete_prefix():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Deleting prefix", "Do you want to delete prefix?") == True:
       shutil.rmtree(homedir + prefixdir)
       tbl.destroy()

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    btn = Button(lf1, text = "Delete prefix", borderwidth = 2, relief = "groove", command = delete_prefix)
    btn.config(font = ("Carlito", 11, "bold"))
    btn.config(fg = "#FFFFFF", bg = "#A50808", activeforeground = "#FFFFFF", activebackground = "#C40707")
    btn.grid(row = 1, column = 4, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf1, text = "Delete prefix", borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 11, "bold"))
    btn.grid(row = 1, column = 4, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def wine_version():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")
    messagebox.showinfo("Wine version", "Current version: " + os.popen("wine --version").read())

if os.path.exists(winepath) == True:
    btn = Button(lf1, text = "Show wine version", borderwidth = 2, relief = "groove", command = wine_version)
    btn.grid(row = 2, column = 4, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf1, text = "Show wine version", borderwidth = 2, relief = "groove", state = "disabled")
    btn.grid(row = 2, column = 4, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------
# Notes:
#	Added in Wine 2.6
#	Enabled by default in Wine 3.2

def csmt_on():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Enabling CSMT", "Do you want to enable CSMT?") == False:
       return

    regfile = open(homedir + "/csmt.reg", "w")
    source = "Windows Registry Editor Version 5.00\n\n[HKEY_CURRENT_USER\Software\Wine\Direct3D]\n""\"csmt\"""=""dword:00000001\n"
    regfile.write(str(source))
    regfile.close()
    print("---\nGlintwine message: Setting CSMT to '1'.\n---")
    os.system("wine regedit " + homedir + "/csmt.reg")
    os.remove(homedir + "/csmt.reg")
    print("---\nGlintwine message: Deleting csmt.reg file.\n---")
    refresh_gui()

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    if "\"csmt\"""=""dword:00000001" in open(homedir + prefixdir + "/user.reg").read() or os.popen("wine --version").read() >= "wine-3.2":
        btn = Button(lf2, text = "Enable", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
        btn.config(font = ("Carlito", 9))
        btn.grid(row = 3, column = 1, padx = 3, pady = 3, sticky = N+S+E+W)
    if "\"csmt\"""=""dword:00000000" in open(homedir + prefixdir + "/user.reg").read() and os.popen("wine --version").read() >= "wine-3.2":
        btn = Button(lf2, text = "Enable", width = 9, borderwidth = 2, relief = "groove", command = csmt_on)
        btn.config(font = ("Carlito", 9))
        btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
        btn.grid(row = 3, column = 1, padx = 3, pady = 3, sticky = N+S+E+W)
    if not "\"csmt\"""=""dword:00000001" in open(homedir + prefixdir + "/user.reg").read() and os.popen("wine --version").read() < "wine-3.2":
        btn = Button(lf2, text = "Enable", width = 9, borderwidth = 2, relief = "groove", command = csmt_on)
        btn.config(font = ("Carlito", 9))
        btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
        btn.grid(row = 3, column = 1, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf2, text = "Enable", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 3, column = 1, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def csmt_off():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Disabling CSMT", "Do you want to disable CSMT?") == False:
       return

    regfile = open(homedir + "/csmt.reg", "w")
    source = "Windows Registry Editor Version 5.00\n\n[HKEY_CURRENT_USER\Software\Wine\Direct3D]\n""\"csmt\"""=""dword:00000000\n"
    regfile.write(str(source))
    regfile.close()
    print("---\nGlintwine message: Setting CSMT to '0'.\n---")
    os.system("wine regedit " + homedir + "/csmt.reg")
    os.remove(homedir + "/csmt.reg")
    print("---\nGlintwine message: Deleting csmt.reg file.\n---")
    refresh_gui()

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    if "\"csmt\"""=""dword:00000000" in open(homedir + prefixdir + "/user.reg").read() or os.popen("wine --version").read() <= "wine-3.2":
        btn = Button(lf2, text = "Disable", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
        btn.config(font = ("Carlito", 9))
        btn.grid(row = 3, column = 2, padx = 3, pady = 3, sticky = N+S+E+W)
    if "\"csmt\"""=""dword:00000001" in open(homedir + prefixdir + "/user.reg").read() and os.popen("wine --version").read() <= "wine-3.2":
        btn = Button(lf2, text = "Disable", width = 9, borderwidth = 2, relief = "groove", command = csmt_off)
        btn.config(font = ("Carlito", 9))
        btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
        btn.grid(row = 3, column = 2, padx = 3, pady = 3, sticky = N+S+E+W)
    if not "\"csmt\"""=""dword:00000000" in open(homedir + prefixdir + "/user.reg").read() and os.popen("wine --version").read() >= "wine-3.2":
        btn = Button(lf2, text = "Disable", width = 9, borderwidth = 2, relief = "groove", command = csmt_off)
        btn.config(font = ("Carlito", 9))
        btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
        btn.grid(row = 3, column = 2, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf2, text = "Disable", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 3, column = 2, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------
# Notes:
#	Added in Wine 3.2
#	Enabled by default in Wine 3.3

def mst_on():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Enabling MultisampleTextures", "Do you want to enable MultisampleTextures?") == False:
       return
    regfile = open(homedir + "/mst.reg", "w")
    source = "Windows Registry Editor Version 5.00\n\n[HKEY_CURRENT_USER\Software\Wine\Direct3D]\n""\"MultisampleTextures\"""=""dword:00000001\n"
    regfile.write(str(source))
    regfile.close()
    print("---\nGlintwine message: Setting MultisampleTextures to '1'.\n---")
    os.system("wine regedit " + homedir + "/mst.reg")
    os.remove(homedir + "/mst.reg")
    print("---\nGlintwine message: Deleting mst.reg file.\n---")
    refresh_gui()

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    if "\"MultisampleTextures\"""=""dword:00000001" in open(homedir + prefixdir + "/user.reg").read() or os.popen("wine --version").read() >= "wine-3.3":
        btn = Button(lf3, text = "Enable", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
        btn.config(font = ("Carlito", 9))
        btn.grid(row = 3, column = 3, padx = 3, pady = 3, sticky = N+S+E+W)
    if "\"MultisampleTextures\"""=""dword:00000000" in open(homedir + prefixdir + "/user.reg").read() and os.popen("wine --version").read() >= "wine-3.3":
        btn = Button(lf3, text = "Enable", width = 9, borderwidth = 2, relief = "groove", command = mst_on)
        btn.config(font = ("Carlito", 9))
        btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
        btn.grid(row = 3, column = 3, padx = 3, pady = 3, sticky = N+S+E+W)
    if not "\"MultisampleTextures\"""=""dword:00000001" in open(homedir + prefixdir + "/user.reg").read() and os.popen("wine --version").read() < "wine-3.3":
        btn = Button(lf3, text = "Enable", width = 9, borderwidth = 2, relief = "groove", command = mst_on)
        btn.config(font = ("Carlito", 9))
        btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
        btn.grid(row = 3, column = 3, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf3, text = "Enable", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 3, column = 3, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def mst_off():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Disabling MultisampleTextures", "Do you want to disable MultisampleTextures?") == False:
       return

    regfile = open(homedir + "/mst.reg", "w")
    source = "Windows Registry Editor Version 5.00\n\n[HKEY_CURRENT_USER\Software\Wine\Direct3D]\n""\"MultisampleTextures\"""=""dword:00000000\n"
    regfile.write(str(source))
    regfile.close()
    print("---\nGlintwine message: Setting MultisampleTextures to '0'.\n---")
    os.system("wine regedit " + homedir + "/mst.reg")
    os.remove(homedir + "/mst.reg")
    print("---\nGlintwine message: Deleting mst.reg file.\n---")
    refresh_gui()

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    if "\"MultisampleTextures\"""=""dword:00000000" in open(homedir + prefixdir + "/user.reg").read() or os.popen("wine --version").read() <= "wine-3.3":
        btn = Button(lf3, text = "Disable", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
        btn.config(font = ("Carlito", 9))
        btn.grid(row = 3, column = 4, padx = 3, pady = 3, sticky = N+S+E+W)
    if "\"MultisampleTextures\"""=""dword:00000001" in open(homedir + prefixdir + "/user.reg").read() and os.popen("wine --version").read() <= "wine-3.3":
        btn = Button(lf3, text = "Disable", width = 9, borderwidth = 2, relief = "groove", command = mst_off)
        btn.config(font = ("Carlito", 9))
        btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
        btn.grid(row = 3, column = 4, padx = 3, pady = 3, sticky = N+S+E+W)
    if not "\"MultisampleTextures\"""=""dword:00000000" in open(homedir + prefixdir + "/user.reg").read() and os.popen("wine --version").read() >= "wine-3.3":
        btn = Button(lf3, text = "Disable", width = 9, borderwidth = 2, relief = "groove", command = mst_off)
        btn.config(font = ("Carlito", 9))
        btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
        btn.grid(row = 3, column = 4, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf3, text = "Disable", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 3, column = 4, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------
# Notes:
#	Added in Wine 4.3
#	See report https://bugs.winehq.org/show_bug.cgi?id=35207 for details.

def ssm_on():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Enabling StrictShaderMath", "Do you want to enable StrictShaderMath?") == False:
       return

    regfile = open(homedir + "/ssm.reg", "w")
    source = "Windows Registry Editor Version 5.00\n\n[HKEY_CURRENT_USER\Software\Wine\Direct3D]\n""\"strict_shader_math\"""=""dword:00000001\n"
    regfile.write(str(source))
    regfile.close()
    print("---\nGlintwine message: Setting StrictShaderMath to '1'.\n---")
    os.system("wine regedit " + homedir + "/ssm.reg")
    os.remove(homedir + "/ssm.reg")
    print("---\nGlintwine message: Deleting ssm.reg file.\n---")
    refresh_gui()

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    if "\"strict_shader_math\"""=""dword:00000001" in open(homedir + prefixdir + "/user.reg").read() or os.popen("wine --version").read() <= "wine-4.3":
        btn = Button(lf4, text = "Enable", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
        btn.config(font = ("Carlito", 9))
        btn.grid(row = 3, column = 5, padx = 3, pady = 3, sticky = N+S+E+W)
    if "\"strict_shader_math\"""=""dword:00000000" in open(homedir + prefixdir + "/user.reg").read() and os.popen("wine --version").read() >= "wine-4.3":
        btn = Button(lf4, text = "Enable", width = 9, borderwidth = 2, relief = "groove", command = ssm_on)
        btn.config(font = ("Carlito", 9))
        btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
        btn.grid(row = 3, column = 5, padx = 3, pady = 3, sticky = N+S+E+W)
    if not "\"strict_shader_math\"""=""dword:00000001" in open(homedir + prefixdir + "/user.reg").read() and os.popen("wine --version").read() >= "wine-4.3":
        btn = Button(lf4, text = "Enable", width = 9, borderwidth = 2, relief = "groove", command = ssm_on)
        btn.config(font = ("Carlito", 9))
        btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
        btn.grid(row = 3, column = 5, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf4, text = "Enable", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 3, column = 5, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def ssm_off():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Disabling StrictShaderMath", "Do you want to disable StrictShaderMath?") == False:
       return

    regfile = open(homedir + "/ssm.reg", "w")
    source = "Windows Registry Editor Version 5.00\n\n[HKEY_CURRENT_USER\Software\Wine\Direct3D]\n""\"strict_shader_math\"""=""dword:00000000\n"
    regfile.write(str(source))
    regfile.close()
    print("---\nGlintwine message: Setting StrictShaderMath to '0'.\n---")
    os.system("wine regedit " + homedir + "/ssm.reg")
    os.remove(homedir + "/ssm.reg")
    print("---\nGlintwine message: Deleting ssm.reg file.\n---")
    refresh_gui()

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    if "\"strict_shader_math\"""=""dword:00000000" in open(homedir + prefixdir + "/user.reg").read() or os.popen("wine --version").read() >= "wine-4.3":
        btn = Button(lf4, text = "Disable", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
        btn.config(font = ("Carlito", 9))
        btn.grid(row = 3, column = 6, padx = 3, pady = 3, sticky = N+S+E+W)
    if "\"strict_shader_math\"""=""dword:00000001" in open(homedir + prefixdir + "/user.reg").read() and os.popen("wine --version").read() >= "wine-4.3":
        btn = Button(lf4, text = "Disable", width = 9, borderwidth = 2, relief = "groove", command = ssm_off)
        btn.config(font = ("Carlito", 9))
        btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
        btn.grid(row = 3, column = 6, padx = 3, pady = 3, sticky = N+S+E+W)
    else:
        btn = Button(lf4, text = "Disable", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
        btn.config(font = ("Carlito", 9))
        btn.grid(row = 3, column = 6, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf4, text = "Disable", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 3, column = 6, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def gor_off():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Disabling gameoverlayrenderer", "Do you want to disable gameoverlayrenderer?") == False:
       return

    regfile = open(homedir + "/gor.reg", "w")
    source = "Windows Registry Editor Version 5.00\n\n[HKEY_CURRENT_USER\Software\Wine\DllOverrides]\n""\"gameoverlayrenderer\"""=""\"\"\n"
    regfile.write(str(source))
    regfile.close()
    print("---\nGlintwine message: Setting gameoverlayrenderer to 'disabled'.\n---")
    os.system("wine regedit " + homedir + "/gor.reg")
    os.remove(homedir + "/gor.reg")
    print("---\nGlintwine message: Deleting gor.reg file.\n---")
    refresh_gui()

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir + steampath) or os.path.exists(homedir + prefixdir + steampath64) == True:
    if "\"gameoverlayrenderer\"""=""\"\"" in open(homedir + prefixdir + "/user.reg").read():
        btn = Button(lf5, text = "Disable", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
        btn.config(font = ("Carlito", 9))
        btn.grid(row = 4, column = 1, padx = 3, pady = 3, sticky = N+S+E+W)
    if not "\"gameoverlayrenderer\"" in open(homedir + prefixdir + "/user.reg").read():
        btn = Button(lf5, text = "Disable", width = 9, borderwidth = 2, relief = "groove", command = gor_off)
        btn.config(font = ("Carlito", 9))
        btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
        btn.grid(row = 4, column = 1, padx = 3, pady = 3, sticky = N+S+E+W)
    if not os.path.exists(winepath):
        btn = Button(lf5, text = "Disable", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
        btn.config(font = ("Carlito", 9))
        btn.grid(row = 4, column = 1, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf5, text = "Disable", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 4, column = 1, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def gor_delete():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Deleting gameoverlayrenderer", "Do you want to delete gameoverlayrenderer?") == False:
       return

    print("---\nGlintwine message: Deleting 'gameoverlayrenderer' registry value.\n---")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v gameoverlayrenderer /f")
    refresh_gui()

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir + steampath) or os.path.exists(homedir + prefixdir + steampath64) == True:
    if "\"gameoverlayrenderer\"""=""\"\"" in open(homedir + prefixdir + "/user.reg").read():
        btn = Button(lf5, text = "Delete", width = 9, borderwidth = 2, relief = "groove", command = gor_delete)
        btn.config(font = ("Carlito", 9))
        btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
        btn.grid(row = 4, column = 2, padx = 3, pady = 3, sticky = N+S+E+W)
    if not "\"gameoverlayrenderer\"""=""\"\"" in open(homedir + prefixdir + "/user.reg").read():
        btn = Button(lf5, text = "Delete", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
        btn.config(font = ("Carlito", 9))
        btn.grid(row = 4, column = 2, padx = 3, pady = 3, sticky = N+S+E+W)
    if not os.path.exists(winepath):
        btn = Button(lf5, text = "Delete", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
        btn.config(font = ("Carlito", 9))
        btn.grid(row = 4, column = 2, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf5, text = "Delete", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 4, column = 2, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def gor64_off():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Disabling gameoverlayrenderer64", "Do you want to disable gameoverlayrenderer64?") == False:
       return

    regfile = open(homedir + "/gor64.reg", "w")
    source = "Windows Registry Editor Version 5.00\n\n[HKEY_CURRENT_USER\Software\Wine\DllOverrides]\n""\"gameoverlayrenderer64\"""=""\"\"\n"
    regfile.write(str(source))
    regfile.close()
    print("---\nGlintwine message: Setting gameoverlayrenderer64 to 'disabled'.\n---")
    os.system("wine regedit " + homedir + "/gor64.reg")
    os.remove(homedir + "/gor64.reg")
    print("---\nGlintwine message: Deleting gor64.reg file.\n---")
    refresh_gui()

if os.path.exists(winepath64) and os.path.exists(homedir + prefixdir + steampath64) == True:
    if "\"gameoverlayrenderer64\"""=""\"\"" in open(homedir + prefixdir + "/user.reg").read():
        btn = Button(lf6, text = "Disable", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
        btn.config(font = ("Carlito", 9))
        btn.grid(row = 4, column = 3, padx = 3, pady = 3, sticky = N+S+E+W)
    if not "\"gameoverlayrenderer64\"" in open(homedir + prefixdir + "/user.reg").read():
        btn = Button(lf6, text = "Disable", width = 9, borderwidth = 2, relief = "groove", command = gor64_off)
        btn.config(font = ("Carlito", 9))
        btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
        btn.grid(row = 4, column = 3, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf6, text = "Disable", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 4, column = 3, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def gor64_delete():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Deleting gameoverlayrenderer64", "Do you want to delete gameoverlayrenderer64?") == False:
       return

    print("---\nGlintwine message: Deleting 'gameoverlayrenderer64' registry value.\n---")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v gameoverlayrenderer64 /f")
    refresh_gui()

if os.path.exists(winepath64) and os.path.exists(homedir + prefixdir + steampath64) == True:
    if "\"gameoverlayrenderer64\"""=""\"\"" in open(homedir + prefixdir + "/user.reg").read():
        btn = Button(lf6, text = "Delete", width = 9, borderwidth = 2, relief = "groove", command = gor64_delete)
        btn.config(font = ("Carlito", 9))
        btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
        btn.grid(row = 4, column = 4, padx = 3, pady = 3, sticky = N+S+E+W)
    if not "\"gameoverlayrenderer64\"""=""\"\"" in open(homedir + prefixdir + "/user.reg").read():
        btn = Button(lf6, text = "Delete", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
        btn.config(font = ("Carlito", 9))
        btn.grid(row = 4, column = 4, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf6, text = "Delete", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 4, column = 4, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def gf_on():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Enabling GrabFullscreen", "Do you want to enable GrabFullscreen?") == False:
       return

    regfile = open(homedir + "/gf.reg", "w")
    source = "Windows Registry Editor Version 5.00\n\n[HKEY_CURRENT_USER\Software\Wine\X11 Driver]\n""\"GrabFullscreen\"""=""\"Y\"""\n"
    regfile.write(str(source))
    regfile.close()
    print("---\nGlintwine message: Setting GrabFullscreen to 'Y'.\n---")
    os.system("wine regedit " + homedir + "/gf.reg")
    os.remove(homedir + "/gf.reg")
    print("---\nGlintwine message: Deleting gf.reg file.\n---")
    refresh_gui()

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    if "\"GrabFullscreen\"""=""\"Y\"""" in open(homedir + prefixdir + "/user.reg").read():
        btn = Button(lf7, text = "Enable", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
        btn.config(font = ("Carlito", 9))
        btn.grid(row = 4, column = 5, padx = 3, pady = 3, sticky = N+S+E+W)
    if not "\"GrabFullscreen\"""=""\"Y\"""" in open(homedir + prefixdir + "/user.reg").read():
        btn = Button(lf7, text = "Enable", width = 9, borderwidth = 2, relief = "groove", command = gf_on)
        btn.config(font = ("Carlito", 9))
        btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
        btn.grid(row = 4, column = 5, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf7, text = "Enable", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 4, column = 5, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def gf_off():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Disabling GrabFullscreen", "Do you want to disable GrabFullscreen?") == False:
       return

    print("---\nGlintwine message: Deleting GrabFullscreen registry string.\n---")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\X11\ Driver /v GrabFullscreen /f")
    refresh_gui()

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    if "\"GrabFullscreen\"""=""\"Y\"""" in open(homedir + prefixdir + "/user.reg").read():
        btn = Button(lf7, text = "Disable", width = 9, borderwidth = 2, relief = "groove", command = gf_off)
        btn.config(font = ("Carlito", 9))
        btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
        btn.grid(row = 4, column = 6, padx = 3, pady = 3, sticky = N+S+E+W)
    if not "\"GrabFullscreen\"""=""\"Y\"""" in open(homedir + prefixdir + "/user.reg").read():
        btn = Button(lf7, text = "Disable", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
        btn.config(font = ("Carlito", 9))
        btn.grid(row = 4, column = 6, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf7, text = "Disable", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 4, column = 6, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def d3dcompiler_builtin():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Setting d3dcompiler_**", "Do you want to set d3dcompiler_** to 'builtin'?") == False:
       return

    regfile = open(homedir + "/d3dcompiler.reg", "w")
    source = "Windows Registry Editor Version 5.00\n\n[HKEY_CURRENT_USER\Software\Wine\DllOverrides]\n""\"d3dcompiler_33\"""=""\"builtin\"\n""\"d3dcompiler_34\"""=""\"builtin\"\n""\"d3dcompiler_35\"""=""\"builtin\"\n""\"d3dcompiler_36\"""=""\"builtin\"\n""\"d3dcompiler_37\"""=""\"builtin\"\n""\"d3dcompiler_38\"""=""\"builtin\"\n""\"d3dcompiler_39\"""=""\"builtin\"\n""\"d3dcompiler_40\"""=""\"builtin\"\n""\"d3dcompiler_41\"""=""\"builtin\"\n""\"d3dcompiler_42\"""=""\"builtin\"\n""\"d3dcompiler_43\"""=""\"builtin\"\n""\"d3dcompiler_46\"""=""\"builtin\"\n""\"d3dcompiler_47\"""=""\"builtin\"\n"
    regfile.write(str(source))
    regfile.close()
    print("---\nGlintwine message: Setting d3dcompiler_** to 'builtin'.\n---")
    os.system("wine regedit " + homedir + "/d3dcompiler.reg")
    os.remove(homedir + "/d3dcompiler.reg")
    print("---\nGlintwine message: Deleting d3dcompiler.reg file.\n---")

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    btn = Button(lf8, text = "Builtin", width = 9, borderwidth = 2, relief = "groove", command = d3dcompiler_builtin)
    btn.config(font = ("Carlito", 9))
    btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
    btn.grid(row = 5, column = 1, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf8, text = "Builtin", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 5, column = 1, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def d3dcompiler_delete():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Deleting d3dcompiler_**", "Do you want to delete d3dcompiler_** overrides?") == False:
       return

    print("---\nGlintwine message: Deleting 'd3dcompiler_**' registry values.\n---")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dcompiler_33 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dcompiler_34 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dcompiler_35 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dcompiler_36 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dcompiler_37 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dcompiler_38 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dcompiler_39 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dcompiler_40 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dcompiler_41 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dcompiler_42 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dcompiler_43 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dcompiler_46 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dcompiler_47 /f")

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    btn = Button(lf8, text = "Delete", width = 9, borderwidth = 2, relief = "groove", command = d3dcompiler_delete)
    btn.config(font = ("Carlito", 9))
    btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
    btn.grid(row = 5, column = 2, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf8, text = "Delete", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 5, column = 2, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def d3dx9_builtin():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Setting d3dx9_**", "Do you want to set d3dx9_** to 'builtin'?") == False:
       return

    regfile = open(homedir + "/d3dx9.reg", "w")
    source = "Windows Registry Editor Version 5.00\n\n[HKEY_CURRENT_USER\Software\Wine\DllOverrides]\n""\"d3dx9_24\"""=""\"builtin\"\n""\"d3dx9_25\"""=""\"builtin\"\n""\"d3dx9_26\"""=""\"builtin\"\n""\"d3dx9_27\"""=""\"builtin\"\n""\"d3dx9_28\"""=""\"builtin\"\n""\"d3dx9_29\"""=""\"builtin\"\n""\"d3dx9_30\"""=""\"builtin\"\n""\"d3dx9_31\"""=""\"builtin\"\n""\"d3dx9_32\"""=""\"builtin\"\n""\"d3dx9_33\"""=""\"builtin\"\n""\"d3dx9_34\"""=""\"builtin\"\n""\"d3dx9_35\"""=""\"builtin\"\n""\"d3dx9_36\"""=""\"builtin\"\n""\"d3dx9_37\"""=""\"builtin\"\n""\"d3dx9_38\"""=""\"builtin\"\n""\"d3dx9_39\"""=""\"builtin\"\n""\"d3dx9_40\"""=""\"builtin\"\n""\"d3dx9_41\"""=""\"builtin\"\n""\"d3dx9_42\"""=""\"builtin\"\n""\"d3dx9_43\"""=""\"builtin\"\n"
    regfile.write(str(source))
    regfile.close()
    print("---\nGlintwine message: Setting d3dx9_** to 'builtin'.\n---")
    os.system("wine regedit " + homedir + "/d3dx9.reg")
    os.remove(homedir + "/d3dx9.reg")
    print("---\nGlintwine message: Deleting d3dx9.reg file.\n---")

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    btn = Button(lf9, text = "Builtin", width = 9, borderwidth = 2, relief = "groove", command = d3dx9_builtin)
    btn.config(font = ("Carlito", 9))
    btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
    btn.grid(row = 5, column = 3, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf9, text = "Builtin", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 5, column = 3, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def d3dx9_delete():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Deleting d3dx9_**", "Do you want to delete d3dx9_** overrides?") == False:
       return

    print("---\nGlintwine message: Deleting 'd3dx9_**' registry values.\n---")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dx9_24 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dx9_25 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dx9_26 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dx9_27 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dx9_28 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dx9_29 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dx9_30 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dx9_31 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dx9_32 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dx9_33 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dx9_34 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dx9_35 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dx9_36 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dx9_37 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dx9_38 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dx9_39 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dx9_40 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dx9_41 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dx9_42 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dx9_43 /f")

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    btn = Button(lf9, text = "Delete", width = 9, borderwidth = 2, relief = "groove", command = d3dx9_delete)
    btn.config(font = ("Carlito", 9))
    btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
    btn.grid(row = 5, column = 4, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf9, text = "Delete", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 5, column = 4, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def d3dx10_builtin():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Setting d3dx10_**", "Do you want to set d3dx10_** to 'builtin'?") == False:
       return

    regfile = open(homedir + "/d3dx10.reg", "w")
    source = "Windows Registry Editor Version 5.00\n\n[HKEY_CURRENT_USER\Software\Wine\DllOverrides]\n""\"d3dx10_33\"""=""\"builtin\"\n""\"d3dx10_34\"""=""\"builtin\"\n""\"d3dx10_35\"""=""\"builtin\"\n""\"d3dx10_36\"""=""\"builtin\"\n""\"d3dx10_37\"""=""\"builtin\"\n""\"d3dx10_38\"""=""\"builtin\"\n""\"d3dx10_39\"""=""\"builtin\"\n""\"d3dx10_40\"""=""\"builtin\"\n""\"d3dx10_41\"""=""\"builtin\"\n""\"d3dx10_42\"""=""\"builtin\"\n""\"d3dx10_43\"""=""\"builtin\"\n"
    regfile.write(str(source))
    regfile.close()
    print("---\nGlintwine message: Setting d3dx10_** to 'builtin'.\n---")
    os.system("wine regedit " + homedir + "/d3dx10.reg")
    os.remove(homedir + "/d3dx10.reg")
    print("---\nGlintwine message: Deleting d3dx10.reg file.\n---")

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    btn = Button(lf10, text = "Builtin", width = 9, borderwidth = 2, relief = "groove", command = d3dx10_builtin)
    btn.config(font = ("Carlito", 9))
    btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
    btn.grid(row = 5, column = 5, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf10, text = "Builtin", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 5, column = 5, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def d3dx10_delete():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Deleting d3dx10_**", "Do you want to delete d3dx10_** overrides?") == False:
       return

    print("---\nGlintwine message: Deleting 'd3dx10_**' registry values.\n---")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dx10_33 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dx10_34 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dx10_35 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dx10_36 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dx10_37 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dx10_38 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dx10_39 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dx10_40 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dx10_41 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dx10_42 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v d3dx10_43 /f")

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    btn = Button(lf10, text = "Delete", width = 9, borderwidth = 2, relief = "groove", command = d3dx10_delete)
    btn.config(font = ("Carlito", 9))
    btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
    btn.grid(row = 5, column = 6, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf10, text = "Delete", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 5, column = 6, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def x3daudio_builtin():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Setting x3daudio*_*", "Do you want to set x3daudio*_* to 'builtin'?") == False:
       return

    regfile = open(homedir + "/x3daudio.reg", "w")
    source = "Windows Registry Editor Version 5.00\n\n[HKEY_CURRENT_USER\Software\Wine\DllOverrides]\n""\"x3daudio1_0\"""=""\"builtin\"\n""\"x3daudio1_1\"""=""\"builtin\"\n""\"x3daudio1_2\"""=""\"builtin\"\n""\"x3daudio1_3\"""=""\"builtin\"\n""\"x3daudio1_4\"""=""\"builtin\"\n""\"x3daudio1_5\"""=""\"builtin\"\n""\"x3daudio1_6\"""=""\"builtin\"\n""\"x3daudio1_7\"""=""\"builtin\"\n"
    regfile.write(str(source))
    regfile.close()
    print("---\nGlintwine message: Setting x3daudio*_* to 'builtin'.\n---")
    os.system("wine regedit " + homedir + "/x3daudio.reg")
    os.remove(homedir + "/x3daudio.reg")
    print("---\nGlintwine message: Deleting x3daudio.reg file.\n---")

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    btn = Button(lf11, text = "Builtin", width = 9, borderwidth = 2, relief = "groove", command = x3daudio_builtin)
    btn.config(font = ("Carlito", 9))
    btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
    btn.grid(row = 6, column = 1, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf11, text = "Builtin", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 6, column = 1, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def x3daudio_delete():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Deleting x3daudio*_*", "Do you want to delete x3daudio*_* overrides?") == False:
       return

    print("---\nGlintwine message: Deleting 'x3daudio*_*' registry values.\n---")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v x3daudio1_0 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v x3daudio1_1 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v x3daudio1_2 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v x3daudio1_3 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v x3daudio1_4 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v x3daudio1_5 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v x3daudio1_6 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v x3daudio1_7 /f")

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    btn = Button(lf11, text = "Delete", width = 9, borderwidth = 2, relief = "groove", command = x3daudio_delete)
    btn.config(font = ("Carlito", 9))
    btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
    btn.grid(row = 6, column = 2, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf11, text = "Delete", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 6, column = 2, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def xapofx_builtin():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Setting xapofx*_*", "Do you want to set xapofx*_* to 'builtin'?") == False:
       return

    regfile = open(homedir + "/xapofx.reg", "w")
    source = "Windows Registry Editor Version 5.00\n\n[HKEY_CURRENT_USER\Software\Wine\DllOverrides]\n""\"xapofx1_1\"""=""\"builtin\"\n""\"xapofx1_2\"""=""\"builtin\"\n""\"xapofx1_3\"""=""\"builtin\"\n""\"xapofx1_4\"""=""\"builtin\"\n""\"xapofx1_5\"""=""\"builtin\"\n"
    regfile.write(str(source))
    regfile.close()
    print("---\nGlintwine message: Setting xapofx*_* to 'builtin'.\n---")
    os.system("wine regedit " + homedir + "/xapofx.reg")
    os.remove(homedir + "/xapofx.reg")
    print("---\nGlintwine message: Deleting xapofx.reg file.\n---")

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    btn = Button(lf12, text = "Builtin", width = 9, borderwidth = 2, relief = "groove", command = xapofx_builtin)
    btn.config(font = ("Carlito", 9))
    btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
    btn.grid(row = 6, column = 3, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf12, text = "Builtin", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 6, column = 3, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def xapofx_delete():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Deleting xapofx*_*", "Do you want to delete xapofx*_* overrides?") == False:
       return

    print("---\nGlintwine message: Deleting 'xapofx*_*' registry values.\n---")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v xapofx1_1 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v xapofx1_2 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v xapofx1_3 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v xapofx1_4 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v xapofx1_5 /f")

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    btn = Button(lf12, text = "Delete", width = 9, borderwidth = 2, relief = "groove", command = xapofx_delete)
    btn.config(font = ("Carlito", 9))
    btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
    btn.grid(row = 6, column = 4, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf12, text = "Delete", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 6, column = 4, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def xaudio_builtin():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Setting xaudio*_*", "Do you want to set xaudio*_* to 'builtin'?") == False:
       return

    regfile = open(homedir + "/xaudio.reg", "w")
    source = "Windows Registry Editor Version 5.00\n\n[HKEY_CURRENT_USER\Software\Wine\DllOverrides]\n""\"xaudio2_0\"""=""\"builtin\"\n""\"xaudio2_1\"""=""\"builtin\"\n""\"xaudio2_2\"""=""\"builtin\"\n""\"xaudio2_3\"""=""\"builtin\"\n""\"xaudio2_4\"""=""\"builtin\"\n""\"xaudio2_5\"""=""\"builtin\"\n""\"xaudio2_6\"""=""\"builtin\"\n""\"xaudio2_7\"""=""\"builtin\"\n""\"xaudio2_8\"""=""\"builtin\"\n""\"xaudio2_9\"""=""\"builtin\"\n"
    regfile.write(str(source))
    regfile.close()
    print("---\nGlintwine message: Setting xaudio*_* to 'builtin'.\n---")
    os.system("wine regedit " + homedir + "/xaudio.reg")
    os.remove(homedir + "/xaudio.reg")
    print("---\nGlintwine message: Deleting xaudio.reg file.\n---")

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    btn = Button(lf13, text = "Builtin", width = 9, borderwidth = 2, relief = "groove", command = xaudio_builtin)
    btn.config(font = ("Carlito", 9))
    btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
    btn.grid(row = 6, column = 5, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf13, text = "Builtin", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 6, column = 5, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def xaudio_delete():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Deleting xaudio*_*", "Do you want to delete xaudio*_* overrides?") == False:
       return

    print("---\nGlintwine message: Deleting 'xaudio*_*' registry values.\n---")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v xaudio2_0 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v xaudio2_1 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v xaudio2_2 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v xaudio2_3 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v xaudio2_4 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v xaudio2_5 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v xaudio2_6 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v xaudio2_7 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v xaudio2_8 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v xaudio2_9 /f")

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    btn = Button(lf13, text = "Delete", width = 9, borderwidth = 2, relief = "groove", command = xaudio_delete)
    btn.config(font = ("Carlito", 9))
    btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
    btn.grid(row = 6, column = 6, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf13, text = "Delete", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 6, column = 6, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def xactengine_builtin():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Setting xactengine*_*", "Do you want to set xactengine*_* to 'builtin'?") == False:
       return

    regfile = open(homedir + "/xactengine.reg", "w")
    source = "Windows Registry Editor Version 5.00\n\n[HKEY_CURRENT_USER\Software\Wine\DllOverrides]\n""\"xactengine2_0\"""=""\"builtin\"\n""\"xactengine2_4\"""=""\"builtin\"\n""\"xactengine2_7\"""=""\"builtin\"\n""\"xactengine2_9\"""=""\"builtin\"\n""\"xactengine3_0\"""=""\"builtin\"\n""\"xactengine3_1\"""=""\"builtin\"\n""\"xactengine3_2\"""=""\"builtin\"\n""\"xactengine3_3\"""=""\"builtin\"\n""\"xactengine3_4\"""=""\"builtin\"\n""\"xactengine3_5\"""=""\"builtin\"\n""\"xactengine3_6\"""=""\"builtin\"\n""\"xactengine3_7\"""=""\"builtin\"\n"
    regfile.write(str(source))
    regfile.close()
    print("---\nGlintwine message: Setting xactengine*_* to 'builtin'.\n---")
    os.system("wine regedit " + homedir + "/xactengine.reg")
    os.remove(homedir + "/xactengine.reg")
    print("---\nGlintwine message: Deleting xactengine.reg file.\n---")

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    btn = Button(lf14, text = "Builtin", width = 9, borderwidth = 2, relief = "groove", command = xactengine_builtin)
    btn.config(font = ("Carlito", 9))
    btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
    btn.grid(row = 7, column = 1, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf14, text = "Builtin", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 7, column = 1, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def xactengine_delete():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Deleting xactengine*_*", "Do you want to delete xactengine*_* overrides?") == False:
       return

    print("---\nGlintwine message: Deleting 'xactengine*_*' registry values.\n---")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v xactengine2_0 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v xactengine2_4 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v xactengine2_7 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v xactengine2_9 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v xactengine3_0 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v xactengine3_1 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v xactengine3_2 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v xactengine3_3 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v xactengine3_4 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v xactengine3_5 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v xactengine3_6 /f")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v xactengine3_7 /f")

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    btn = Button(lf14, text = "Delete", width = 9, borderwidth = 2, relief = "groove", command = xactengine_delete)
    btn.config(font = ("Carlito", 9))
    btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
    btn.grid(row = 7, column = 2, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf14, text = "Delete", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 7, column = 2, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def mf_disable():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Disabling mf*", "Do you want to disable mf*?") == False:
       return

    regfile = open(homedir + "/mf.reg", "w")
    source = "Windows Registry Editor Version 5.00\n\n[HKEY_CURRENT_USER\Software\Wine\DllOverrides]\n""\"mfplat\"""=""\"\"\n"
    regfile.write(str(source))
    regfile.close()
    print("---\nGlintwine message: Disabling mf*.\n---")
    os.system("wine regedit " + homedir + "/mf.reg")
    os.remove(homedir + "/mf.reg")
    print("---\nGlintwine message: Deleting mf.reg file.\n---")

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    btn = Button(lf15, text = "Disable", width = 9, borderwidth = 2, relief = "groove", command = mf_disable)
    btn.config(font = ("Carlito", 9))
    btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
    btn.grid(row = 7, column = 3, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf15, text = "Disable", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 7, column = 3, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def mf_delete():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Deleting mf*", "Do you want to delete mf* override?") == False:
       return

    print("---\nGlintwine message: Deleting 'mf*' registry value.\n---")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\DllOverrides /v mfplat /f")

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    btn = Button(lf15, text = "Delete", width = 9, borderwidth = 2, relief = "groove", command = mf_delete)
    btn.config(font = ("Carlito", 9))
    btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
    btn.grid(row = 7, column = 4, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf15, text = "Delete", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 7, column = 4, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def vkd3d_on():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Setting Vulkan renderer", "Do you want to set renderer to 'vulkan'?") == False:
       return

    regfile = open(homedir + "/vkd3d.reg", "w")
    source = "Windows Registry Editor Version 5.00\n\n[HKEY_CURRENT_USER\Software\Wine\Direct3D]\n""\"renderer\"""=""\"vulkan\"""\n"
    regfile.write(str(source))
    regfile.close()
    print("---\nGlintwine message: Setting renderer to 'vulkan'.\n---")
    os.system("wine regedit " + homedir + "/vkd3d.reg")
    os.remove(homedir + "/vkd3d.reg")
    print("---\nGlintwine message: Deleting vkd3d.reg file.\n---")
    refresh_gui()

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    if "\"renderer\"""=""\"vulkan\"""" in open(homedir + prefixdir + "/user.reg").read():
        btn = Button(lf16, text = "Enable", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
        btn.config(font = ("Carlito", 9))
        btn.grid(row = 7, column = 5, padx = 3, pady = 3, sticky = N+S+E+W)
    if not "\"renderer\"""=""\"vulkan\"""" in open(homedir + prefixdir + "/user.reg").read():
        btn = Button(lf16, text = "Enable", width = 9, borderwidth = 2, relief = "groove", command = vkd3d_on)
        btn.config(font = ("Carlito", 9))
        btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
        btn.grid(row = 7, column = 5, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf16, text = "Enable", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 7, column = 5, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def vkd3d_off():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Deleting Vulkan renderer", "Do you want to set renderer by default?") == False:
       return

    print("---\nGlintwine message: Deleting 'vulkan' renderer string.\n---")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\Direct3D /v renderer /f")
    refresh_gui()

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    if "\"renderer\"""=""\"vulkan\"""" in open(homedir + prefixdir + "/user.reg").read():
        btn = Button(lf16, text = "Disable", width = 9, borderwidth = 2, relief = "groove", command = vkd3d_off)
        btn.config(font = ("Carlito", 9))
        btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
        btn.grid(row = 7, column = 6, padx = 3, pady = 3, sticky = N+S+E+W)
    if not "\"renderer\"""=""\"vulkan\"""" in open(homedir + prefixdir + "/user.reg").read():
        btn = Button(lf16, text = "Disable", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
        btn.config(font = ("Carlito", 9))
        btn.grid(row = 7, column = 6, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf16, text = "Disable", width = 9, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 7, column = 6, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def vram_auto():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Deleting VideoMemorySize", "Do you want to delete VideoMemorySize?") == False:
       return

    print("---\nGlintwine message: Deleting 'VideoMemorySize' registry value.\n---")
    os.system("wine reg delete HKEY_CURRENT_USER\\\Software\\\Wine\\\Direct3D /v VideoMemorySize /f")
    refresh_gui()

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    if "VideoMemorySize" in open(homedir + prefixdir + "/user.reg").read():
        btn = Button(lf17, text = "Auto", width = 5, borderwidth = 2, relief = "groove", command = vram_auto)
        btn.config(font = ("Carlito", 9))
        btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
        btn.grid(row = 8, column = 1, padx = 3, pady = 3, sticky = N+S+E+W)
    else:
        btn = Button(lf17, text = "Auto", width = 5, borderwidth = 2, relief = "groove", state = "disabled")
        btn.config(font = ("Carlito", 9))
        btn.grid(row = 8, column = 1, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf17, text = "Auto", width = 5, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 8, column = 1, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def vram_512():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Setting VideoMemorySize", "Do you want to set VideoMemorySize to '512'?") == False:
       return

    regfile = open(homedir + "/vram_512.reg", "w")
    source = "Windows Registry Editor Version 5.00\n\n[HKEY_CURRENT_USER\Software\Wine\Direct3D]\n""\"VideoMemorySize\"""=""\"512\"""\n"
    regfile.write(str(source))
    regfile.close()
    print("---\nGlintwine message: Setting VideoMemorySize to '512'.\n---")
    os.system("wine regedit " + homedir + "/vram_512.reg")
    os.remove(homedir + "/vram_512.reg")
    print("---\nGlintwine message: Deleting vram_512.reg file.\n---")
    refresh_gui()

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    if not "\"VideoMemorySize\"""=""\"512\"" in open(homedir + prefixdir + "/user.reg").read():
        btn = Button(lf17, text = "512", width = 5, borderwidth = 2, relief = "groove", command = vram_512)
        btn.config(font = ("Carlito", 9))
        btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
        btn.grid(row = 8, column = 2, padx = 3, pady = 3, sticky = N+S+E+W)
    if "\"VideoMemorySize\"""=""\"512\"" in open(homedir + prefixdir + "/user.reg").read():
        btn = Button(lf17, text = "512", width = 5, borderwidth = 2, relief = "groove", state = "disabled")
        btn.config(font = ("Carlito", 9))
        btn.grid(row = 8, column = 2, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf17, text = "512", width = 5, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 8, column = 2, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def vram_1024():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Setting VideoMemorySize", "Do you want to set VideoMemorySize to '1024'?") == False:
       return

    regfile = open(homedir + "/vram_1024.reg", "w")
    source = "Windows Registry Editor Version 5.00\n\n[HKEY_CURRENT_USER\Software\Wine\Direct3D]\n""\"VideoMemorySize\"""=""\"1024\"""\n"
    regfile.write(str(source))
    regfile.close()
    print("---\nGlintwine message: Setting VideoMemorySize to '1024'.\n---")
    os.system("wine regedit " + homedir + "/vram_1024.reg")
    os.remove(homedir + "/vram_1024.reg")
    print("---\nGlintwine message: Deleting vram_1024.reg file.\n---")
    refresh_gui()

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    if not "\"VideoMemorySize\"""=""\"1024\"" in open(homedir + prefixdir + "/user.reg").read():
        btn = Button(lf17, text = "1024", width = 5, borderwidth = 2, relief = "groove", command = vram_1024)
        btn.config(font = ("Carlito", 9))
        btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
        btn.grid(row = 8, column = 3, padx = 3, pady = 3, sticky = N+S+E+W)
    if "\"VideoMemorySize\"""=""\"1024\"" in open(homedir + prefixdir + "/user.reg").read():
        btn = Button(lf17, text = "1024", width = 5, borderwidth = 2, relief = "groove", state = "disabled")
        btn.config(font = ("Carlito", 9))
        btn.grid(row = 8, column = 3, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf17, text = "1024", width = 5, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 8, column = 3, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def vram_2048():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Setting VideoMemorySize", "Do you want to set VideoMemorySize to '2048'?") == False:
       return

    regfile = open(homedir + "/vram_2048.reg", "w")
    source = "Windows Registry Editor Version 5.00\n\n[HKEY_CURRENT_USER\Software\Wine\Direct3D]\n""\"VideoMemorySize\"""=""\"2048\"""\n"
    regfile.write(str(source))
    regfile.close()
    print("---\nGlintwine message: Setting VideoMemorySize to '2048'.\n---")
    os.system("wine regedit " + homedir + "/vram_2048.reg")
    os.remove(homedir + "/vram_2048.reg")
    print("---\nGlintwine message: Deleting vram_2048.reg file.\n---")
    refresh_gui()

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    if not "\"VideoMemorySize\"""=""\"2048\"" in open(homedir + prefixdir + "/user.reg").read():
        btn = Button(lf17, text = "2048", width = 5, borderwidth = 2, relief = "groove", command = vram_2048)
        btn.config(font = ("Carlito", 9))
        btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
        btn.grid(row = 8, column = 4, padx = 3, pady = 3, sticky = N+S+E+W)
    if "\"VideoMemorySize\"""=""\"2048\"" in open(homedir + prefixdir + "/user.reg").read():
        btn = Button(lf17, text = "2048", width = 5, borderwidth = 2, relief = "groove", state = "disabled")
        btn.config(font = ("Carlito", 9))
        btn.grid(row = 8, column = 4, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf17, text = "2048", width = 5, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 8, column = 4, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def vram_4096():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Setting VideoMemorySize", "Do you want to set VideoMemorySize to '4096'?") == False:
       return

    regfile = open(homedir + "/vram_4096.reg", "w")
    source = "Windows Registry Editor Version 5.00\n\n[HKEY_CURRENT_USER\Software\Wine\Direct3D]\n""\"VideoMemorySize\"""=""\"4096\"""\n"
    regfile.write(str(source))
    regfile.close()
    print("---\nGlintwine message: Setting VideoMemorySize to '4096'.\n---")
    os.system("wine regedit " + homedir + "/vram_4096.reg")
    os.remove(homedir + "/vram_4096.reg")
    print("---\nGlintwine message: Deleting vram_4096.reg file.\n---")
    refresh_gui()

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    if not "\"VideoMemorySize\"""=""\"4096\"" in open(homedir + prefixdir + "/user.reg").read():
        btn = Button(lf17, text = "4096", width = 5, borderwidth = 2, relief = "groove", command = vram_4096)
        btn.config(font = ("Carlito", 9))
        btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
        btn.grid(row = 8, column = 5, padx = 3, pady = 3, sticky = N+S+E+W)
    if "\"VideoMemorySize\"""=""\"4096\"" in open(homedir + prefixdir + "/user.reg").read():
        btn = Button(lf17, text = "4096", width = 5, borderwidth = 2, relief = "groove", state = "disabled")
        btn.config(font = ("Carlito", 9))
        btn.grid(row = 8, column = 5, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf17, text = "4096", width = 5, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 8, column = 5, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def vram_6144():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Setting VideoMemorySize", "Do you want to set VideoMemorySize to '6144'?") == False:
       return

    regfile = open(homedir + "/vram_6144.reg", "w")
    source = "Windows Registry Editor Version 5.00\n\n[HKEY_CURRENT_USER\Software\Wine\Direct3D]\n""\"VideoMemorySize\"""=""\"6144\"""\n"
    regfile.write(str(source))
    regfile.close()
    print("---\nGlintwine message: Setting VideoMemorySize to '6144'.\n---")
    os.system("wine regedit " + homedir + "/vram_6144.reg")
    os.remove(homedir + "/vram_6144.reg")
    print("---\nGlintwine message: Deleting vram_6144.reg file.\n---")
    refresh_gui()

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    if not "\"VideoMemorySize\"""=""\"6144\"" in open(homedir + prefixdir + "/user.reg").read():
        btn = Button(lf17, text = "6144", width = 5, borderwidth = 2, relief = "groove", command = vram_6144)
        btn.config(font = ("Carlito", 9))
        btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
        btn.grid(row = 8, column = 6, padx = 3, pady = 3, sticky = N+S+E+W)
    if "\"VideoMemorySize\"""=""\"6144\"" in open(homedir + prefixdir + "/user.reg").read():
        btn = Button(lf17, text = "6144", width = 5, borderwidth = 2, relief = "groove", state = "disabled")
        btn.config(font = ("Carlito", 9))
        btn.grid(row = 8, column = 6, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf17, text = "6144", width = 5, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 8, column = 6, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def vram_8192():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Setting VideoMemorySize", "Do you want to set VideoMemorySize to '8192'?") == False:
       return

    regfile = open(homedir + "/vram_8192.reg", "w")
    source = "Windows Registry Editor Version 5.00\n\n[HKEY_CURRENT_USER\Software\Wine\Direct3D]\n""\"VideoMemorySize\"""=""\"8192\"""\n"
    regfile.write(str(source))
    regfile.close()
    print("---\nGlintwine message: Setting VideoMemorySize to '8192'.\n---")
    os.system("wine regedit " + homedir + "/vram_8192.reg")
    os.remove(homedir + "/vram_8192.reg")
    print("---\nGlintwine message: Deleting vram_8192.reg file.\n---")
    refresh_gui()

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    if not "\"VideoMemorySize\"""=""\"8192\"" in open(homedir + prefixdir + "/user.reg").read():
        btn = Button(lf17, text = "8192", width = 5, borderwidth = 2, relief = "groove", command = vram_8192)
        btn.config(font = ("Carlito", 9))
        btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
        btn.grid(row = 8, column = 7, padx = 3, pady = 3, sticky = N+S+E+W)
    if "\"VideoMemorySize\"""=""\"8192\"" in open(homedir + prefixdir + "/user.reg").read():
        btn = Button(lf17, text = "8192", width = 5, borderwidth = 2, relief = "groove", state = "disabled")
        btn.config(font = ("Carlito", 9))
        btn.grid(row = 8, column = 7, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf17, text = "8192", width = 5, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 8, column = 7, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def vram_11264():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Setting VideoMemorySize", "Do you want to set VideoMemorySize to '11264'?") == False:
       return

    regfile = open(homedir + "/vram_11264.reg", "w")
    source = "Windows Registry Editor Version 5.00\n\n[HKEY_CURRENT_USER\Software\Wine\Direct3D]\n""\"VideoMemorySize\"""=""\"11264\"""\n"
    regfile.write(str(source))
    regfile.close()
    print("---\nGlintwine message: Setting VideoMemorySize to '11264'.\n---")
    os.system("wine regedit " + homedir + "/vram_11264.reg")
    os.remove(homedir + "/vram_11264.reg")
    print("---\nGlintwine message: Deleting vram_11264.reg file.\n---")
    refresh_gui()

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    if not "\"VideoMemorySize\"""=""\"11264\"" in open(homedir + prefixdir + "/user.reg").read():
        btn = Button(lf17, text = "11264", width = 5, borderwidth = 2, relief = "groove", command = vram_11264)
        btn.config(font = ("Carlito", 9))
        btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
        btn.grid(row = 8, column = 8, padx = 3, pady = 3, sticky = N+S+E+W)
    if "\"VideoMemorySize\"""=""\"11264\"" in open(homedir + prefixdir + "/user.reg").read():
        btn = Button(lf17, text = "11264", width = 5, borderwidth = 2, relief = "groove", state = "disabled")
        btn.config(font = ("Carlito", 9))
        btn.grid(row = 8, column = 8, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf17, text = "11264", width = 5, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 8, column = 8, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

def vram_12288():
    tbl.option_add("*Dialog.msg.font", "Carlito 11")

    if messagebox.askyesno("Setting VideoMemorySize", "Do you want to set VideoMemorySize to '12288'?") == False:
       return

    regfile = open(homedir + "/vram_12288.reg", "w")
    source = "Windows Registry Editor Version 5.00\n\n[HKEY_CURRENT_USER\Software\Wine\Direct3D]\n""\"VideoMemorySize\"""=""\"12288\"""\n"
    regfile.write(str(source))
    regfile.close()
    print("---\nGlintwine message: Setting VideoMemorySize to '12288'.\n---")
    os.system("wine regedit " + homedir + "/vram_12288.reg")
    os.remove(homedir + "/vram_12288.reg")
    print("---\nGlintwine message: Deleting vram_12288.reg file.\n---")
    refresh_gui()

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    if not "\"VideoMemorySize\"""=""\"12288\"" in open(homedir + prefixdir + "/user.reg").read():
        btn = Button(lf17, text = "12288", width = 5, borderwidth = 2, relief = "groove", command = vram_12288)
        btn.config(font = ("Carlito", 9))
        btn.config(fg = "#FFFFFF", bg = "#869091", activeforeground = "#FFFFFF", activebackground = "#929FA1")
        btn.grid(row = 8, column = 9, padx = 3, pady = 3, sticky = N+S+E+W)
    if "\"VideoMemorySize\"""=""\"12288\"" in open(homedir + prefixdir + "/user.reg").read():
        btn = Button(lf17, text = "12288", width = 5, borderwidth = 2, relief = "groove", state = "disabled")
        btn.config(font = ("Carlito", 9))
        btn.grid(row = 8, column = 9, padx = 3, pady = 3, sticky = N+S+E+W)
else:
    btn = Button(lf17, text = "12288", width = 5, borderwidth = 2, relief = "groove", state = "disabled")
    btn.config(font = ("Carlito", 9))
    btn.grid(row = 8, column = 9, padx = 3, pady = 3, sticky = N+S+E+W)

# ---------------------------------------------------------------------------------------

if os.path.exists(winepath) and os.path.exists(homedir + prefixdir) == True:
    if "#arch=win64" in open(homedir + prefixdir + "/user.reg").read() and not os.path.exists(winepath64):
        tbl.option_add("*Dialog.msg.font", "Carlito 11")
        messagebox.showwarning("Warning", "64-bit prefix with 32-bit Wine isn't acceptable. Please be sure you have installed WoW64.")
        tbl.destroy()

tbl.mainloop()
